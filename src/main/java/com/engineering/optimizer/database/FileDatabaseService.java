package com.engineering.optimizer.database;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.engineering.optimizer.FileUtils.getRowsFromFile;

public class FileDatabaseService {

    public List<Order> getOrdersFromFile(String path) {
        List<String> rows = getRowsFromFile(path);
        List<Order> result = new ArrayList<>();
        for (String row : rows) {
            result.add(createOrder(prepareDataForEntity(row)));
        }
        return result;
    }

    public List<Lineitem> getLineitemsFromFile(String path) {
        List<String> rows = getRowsFromFile(path);
        List<Lineitem> result = new ArrayList<>();
        for (String row : rows) {
            result.add(createLineitem(prepareDataForEntity(row)));
        }
        return result;
    }

    public List<Integer> getIdsForDeleting(String path) {
        List<String> rows = getRowsFromFile(path);
        List<Integer> result = new ArrayList<>();
        for (String row : rows) {
            result.add(Integer.parseInt(prepareDataForEntity(row).get(0)));
        }
        return result;
    }

    private List<String> prepareDataForEntity(String row) {
        return Arrays.asList(row.split("\\|"));
    }

    private Order createOrder(List<String> dataForOrder) {
        return new Order(Integer.parseInt(dataForOrder.get(0)), Integer.parseInt(dataForOrder.get(1)),
                dataForOrder.get(2), new BigDecimal(dataForOrder.get(3)), Date.valueOf(dataForOrder.get(4)),
                dataForOrder.get(5), dataForOrder.get(6), Integer.parseInt(dataForOrder.get(7)), dataForOrder.get(8));
    }

    private Lineitem createLineitem(List<String> dataForLineitem) {
        return new Lineitem(Integer.parseInt(dataForLineitem.get(0)), Integer.parseInt(dataForLineitem.get(1)),
                Integer.parseInt(dataForLineitem.get(2)), Integer.parseInt(dataForLineitem.get(3)),
                new BigDecimal(dataForLineitem.get(4)), new BigDecimal(dataForLineitem.get(5)),
                new BigDecimal(dataForLineitem.get(6)), new BigDecimal(dataForLineitem.get(7)),
                dataForLineitem.get(8), dataForLineitem.get(9), Date.valueOf(dataForLineitem.get(10)),
                Date.valueOf(dataForLineitem.get(11)), Date.valueOf(dataForLineitem.get(12)),
                dataForLineitem.get(13), dataForLineitem.get(14), dataForLineitem.get(15));
    }
}
