package com.engineering.optimizer.database;

import java.math.BigDecimal;
import java.sql.Date;

public class Lineitem {

    private final int orderkey;
    private final int partkey;
    private final int suppkey;
    private final int linenumber;
    private final BigDecimal quantity;
    private final BigDecimal extendedprice;
    private final BigDecimal discount;
    private final BigDecimal tax;
    private final String returnflag;
    private final String linestatus;
    private final Date shipdate;
    private final Date commitdate;
    private final Date receiptdate;
    private final String shipinstruct;
    private final String shipmode;
    private final String comment;

    public Lineitem(int orderkey, int partkey, int suppkey, int linenumber, BigDecimal quantity,
                    BigDecimal extendedprice, BigDecimal discount, BigDecimal tax, String returnflag,
                    String linestatus, Date shipdate, Date commitdate, Date receiptdate, String shipinstruct,
                    String shipmode, String comment) {
        this.orderkey = orderkey;
        this.partkey = partkey;
        this.suppkey = suppkey;
        this.linenumber = linenumber;
        this.quantity = quantity;
        this.extendedprice = extendedprice;
        this.discount = discount;
        this.tax = tax;
        this.returnflag = returnflag;
        this.linestatus = linestatus;
        this.shipdate = shipdate;
        this.commitdate = commitdate;
        this.receiptdate = receiptdate;
        this.shipinstruct = shipinstruct;
        this.shipmode = shipmode;
        this.comment = comment;
    }

    public int getOrderkey() {
        return orderkey;
    }

    public int getPartkey() {
        return partkey;
    }

    public int getSuppkey() {
        return suppkey;
    }

    public int getLinenumber() {
        return linenumber;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getExtendedprice() {
        return extendedprice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public String getReturnflag() {
        return returnflag;
    }

    public String getLinestatus() {
        return linestatus;
    }

    public Date getShipdate() {
        return shipdate;
    }

    public Date getCommitdate() {
        return commitdate;
    }

    public Date getReceiptdate() {
        return receiptdate;
    }

    public String getShipinstruct() {
        return shipinstruct;
    }

    public String getShipmode() {
        return shipmode;
    }

    public String getComment() {
        return comment;
    }
}
