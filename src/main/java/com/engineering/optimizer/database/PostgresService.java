package com.engineering.optimizer.database;

import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;
import java.util.stream.Collectors;

import static com.engineering.optimizer.database.Query.ALL_QUERIES;
import static com.engineering.optimizer.database.SQLText.*;

@Service
public class PostgresService {

    private final Connection connection;
    private final FileDatabaseService fileDatabaseService;

    private static final String LINEITEM_UPDATE_PATH = "src/main/resources/static/lineitem.tbl.u";
    private static final String ORDERS_UPDATE_PATH = "src/main/resources/static/orders.tbl.u";
    private static final String DELETE_PATH = "src/main/resources/static/delete.";

    public PostgresService() throws SQLException {

        String url = "jdbc:postgresql://localhost:5432/tpch";
        String username = "postgres";
        String password = "studyProject";

        this.connection = DriverManager.getConnection(url, username, password);
        this.fileDatabaseService = new FileDatabaseService();
    }

    public void executeQueries() {
        for (Query query : ALL_QUERIES) {
            executeTpchQuery(query);
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException sqlException){
            System.out.println("Cannot close database connection: " + sqlException.getMessage());
        }
    }

    public int getRefreshIterator() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(GET_REFRESH_ITERATOR_VALUE);
        resultSet.next();
        return resultSet.getInt("nextval");
    }

    public void executeFirstRefreshFunction(int refreshIterator) {
        addRowsToOrdersFromFile(String.valueOf(refreshIterator));
        addRowsToLineitemFromFile(String.valueOf(refreshIterator));
    }

    public void executeSecondRefreshFunction(int refreshIterator) throws SQLException {
        deleteRowsFromOrdersAndLineitemByIdsFromFile(String.valueOf(refreshIterator));
    }

    public void addRowsToOrdersFromFile(String fileIndex) {
        List<Order> ordersToAdd = fileDatabaseService.getOrdersFromFile(ORDERS_UPDATE_PATH + fileIndex);
        for (Order order : ordersToAdd) {
            insertOrder(order);
        }
        System.out.println("orders to add: " +
                ordersToAdd.stream().map(Order::getOrderkey).collect(Collectors.toList()));
    }

    public void addRowsToLineitemFromFile(String fileIndex) {
        List<Lineitem> lineitemsToAdd = fileDatabaseService.getLineitemsFromFile(LINEITEM_UPDATE_PATH + fileIndex);
        for (Lineitem lineitem : lineitemsToAdd) {
            insertLineitem(lineitem);
        }
        System.out.println("lineitems to add: " +
                lineitemsToAdd.stream().map(Lineitem::getOrderkey).collect(Collectors.toList()));
    }

    public void deleteRowsFromOrdersAndLineitemByIdsFromFile(String fileIndex) throws SQLException {
        List<Integer> idsForDeleting = fileDatabaseService.getIdsForDeleting(DELETE_PATH + fileIndex);
        String setOfIdsForDeleting = prepareSetOfIdsToDelete(idsForDeleting);
        System.out.println("set of ids to delete: " + setOfIdsForDeleting);
        deleteRowsFromLineitemByIdsFromSet(setOfIdsForDeleting);
        deleteRowsFromOrdersByIdsFromSet(setOfIdsForDeleting);
    }

    public ResultSet findOrdersById(int i) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_ORDER_BY_ID);
        preparedStatement.setInt(1, i);
        return preparedStatement.executeQuery();
    }

    public ResultSet findLineitemsById(int i) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_LINEITEM_BY_ID);
        preparedStatement.setInt(1, i);
        return preparedStatement.executeQuery();
    }

    private String prepareSetOfIdsToDelete(List<Integer> idsForDeleting) {
        StringBuilder setOfIdsForDeleting = new StringBuilder();
        setOfIdsForDeleting.append("(");
        for (int i = 0; i < idsForDeleting.size() - 1; i++) {
            setOfIdsForDeleting.append(idsForDeleting.get(i));
            setOfIdsForDeleting.append(",");
            setOfIdsForDeleting.append(" ");
        }
        setOfIdsForDeleting.append(idsForDeleting.get(idsForDeleting.size()-1));
        setOfIdsForDeleting.append( ")");
        return setOfIdsForDeleting.toString();
    }

    private void deleteRowsFromOrdersByIdsFromSet(String setOfIds) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(DELETE_ROW_FROM_ORDERS + setOfIds);
    }

    private void deleteRowsFromLineitemByIdsFromSet(String setOfIds) throws SQLException {
        Statement statement = connection.createStatement();
        statement.executeUpdate(DELETE_ROW_FROM_LINEITEM + setOfIds);
    }

    private void executeTpchQuery(Query query) {
        try {
            Statement statement = connection.createStatement();
            System.out.println("Executing query: " + query.getName() + "/22 ... ");
            statement.executeQuery(query.getText());
        } catch (SQLException sqlException){
            System.out.println("Cannot execute query: " + query.getName() + " :" + sqlException.getMessage());
            sqlException.printStackTrace();
        }
    }

    private void insertOrder(Order order) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(ADD_ORDER);
            setPreparedStatement(order, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException sqlException) {
            System.out.println("Cannot insert into orders :" + sqlException.getMessage());
            sqlException.printStackTrace();
        }
    }

    private void insertLineitem(Lineitem lineitem){
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(ADD_LINEITEM);
            setPreparedStatement(lineitem, preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException sqlException) {
            System.out.println("Cannot insert into lineitem :" + sqlException.getMessage());
        }
    }

    private void setPreparedStatement(Order order, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, order.getOrderkey());
        preparedStatement.setInt(2, order.getCustkey());
        preparedStatement.setString(3, order.getOrderstatus());
        preparedStatement.setBigDecimal(4, order.getTotalprice());
        preparedStatement.setDate(5, order.getOrderdate());
        preparedStatement.setString(6, order.getOrderpriority());
        preparedStatement.setString(7, order.getClerk());
        preparedStatement.setInt(8, order.getShippriority());
        preparedStatement.setString(9, order.getComment());
    }

    private void setPreparedStatement(Lineitem lineitem, PreparedStatement preparedStatement) throws SQLException {
        preparedStatement.setInt(1, lineitem.getOrderkey());
        preparedStatement.setInt(2, lineitem.getPartkey());
        preparedStatement.setInt(3, lineitem.getSuppkey());
        preparedStatement.setInt(4, lineitem.getLinenumber());
        preparedStatement.setBigDecimal(5, lineitem.getQuantity());
        preparedStatement.setBigDecimal(6, lineitem.getExtendedprice());
        preparedStatement.setBigDecimal(7, lineitem.getDiscount());
        preparedStatement.setBigDecimal(8, lineitem.getTax());
        preparedStatement.setString(9, lineitem.getReturnflag());
        preparedStatement.setString(10, lineitem.getLinestatus());
        preparedStatement.setDate(11, lineitem.getShipdate());
        preparedStatement.setDate(12, lineitem.getCommitdate());
        preparedStatement.setDate(13, lineitem.getReceiptdate());
        preparedStatement.setString(14, lineitem.getShipinstruct());
        preparedStatement.setString(15, lineitem.getShipmode());
        preparedStatement.setString(16, lineitem.getComment());
    }
}
