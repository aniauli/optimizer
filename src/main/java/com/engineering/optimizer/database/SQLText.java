package com.engineering.optimizer.database;

public class SQLText {

    public static final String ADD_ORDER = "INSERT INTO orders (o_orderkey, o_custkey, o_orderstatus, o_totalprice, " +
            "o_orderdate, o_orderpriority, o_clerk, o_shippriority, o_comment) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    public static final String ADD_LINEITEM = "INSERT INTO lineitem (l_orderkey, l_partkey, l_suppkey, l_linenumber, " +
            "l_quantity, l_extendedprice, l_discount, l_tax, l_returnflag, l_linestatus, l_shipdate, l_commitdate," +
            "l_receiptdate, l_shipinstruct, l_shipmode, l_comment) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    public static final String DELETE_ROW_FROM_ORDERS = "DELETE FROM orders WHERE o_orderkey IN ";
    public static final String DELETE_ROW_FROM_LINEITEM = "DELETE FROM lineitem WHERE l_orderkey IN ";

    public static final String GET_REFRESH_ITERATOR_VALUE = "SELECT nextval('refreshIterator')";

    public static final String FIND_ORDER_BY_ID = "SELECT * FROM orders WHERE orders.o_orderkey = ?";
    public static final String FIND_LINEITEM_BY_ID = "SELECT * FROM lineitem WHERE lineitem.l_orderkey = ?";

}
