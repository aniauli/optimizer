package com.engineering.optimizer.database;

import java.math.BigDecimal;
import java.sql.Date;

public class Order {

   private final int orderkey;
   private final int custkey;
   private final String orderstatus;
   private final BigDecimal totalprice;
   private final Date orderdate;
   private final String orderpriority;
   private final String clerk;
   private final int shippriority;
   private final String comment;

    public Order(int orderkey, int custkey, String orderstatus, BigDecimal totalprice, Date orderdate,
                 String orderpriority, String clerk, int shippriority, String comment) {
        this.orderkey = orderkey;
        this.custkey = custkey;
        this.orderstatus = orderstatus;
        this.totalprice = totalprice;
        this.orderdate = orderdate;
        this.orderpriority = orderpriority;
        this.clerk = clerk;
        this.shippriority = shippriority;
        this.comment = comment;
    }

    public int getOrderkey() {
        return orderkey;
    }

    public int getCustkey() {
        return custkey;
    }

    public String getOrderstatus() {
        return orderstatus;
    }

    public BigDecimal getTotalprice() {
        return totalprice;
    }

    public Date getOrderdate() {
        return orderdate;
    }

    public String getOrderpriority() {
        return orderpriority;
    }

    public String getClerk() {
        return clerk;
    }

    public int getShippriority() {
        return shippriority;
    }

    public String getComment() {
        return comment;
    }
}
