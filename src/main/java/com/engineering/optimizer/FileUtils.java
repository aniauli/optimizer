package com.engineering.optimizer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    public static List<String> getRowsFromFile(String path) {
        List<String> result = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            String line = reader.readLine();
            while (null != line) {
                result.add(line);
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException ioException){
            System.out.println("Cannot read from file: " + path + " " + ioException.getMessage());
        }
        return result;
    }
}
