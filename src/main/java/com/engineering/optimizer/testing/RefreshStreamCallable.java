package com.engineering.optimizer.testing;

import com.engineering.optimizer.database.PostgresService;

import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Callable;

public class RefreshStreamCallable implements Callable<Long> {
    private final int refreshIterator;

    RefreshStreamCallable(int refreshIterator) {
        this.refreshIterator = refreshIterator;
        System.out.println("Creating thread with refresh iterator: " + refreshIterator);
    }

    @Override
    public Long call() throws Exception {
        Instant startTime = Instant.now();
        try {
            PostgresService postgresService = new PostgresService();
            postgresService.executeFirstRefreshFunction(refreshIterator);
            postgresService.executeSecondRefreshFunction(refreshIterator);
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        Instant stopTime = Instant.now();
        return Duration.between(startTime, stopTime).toMillis();
    }
}