package com.engineering.optimizer.testing;

import com.engineering.optimizer.database.PostgresService;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.*;

@Service
public class TestService {

    private static final int REFRESH_ITERATOR_MAX_VALUE = 40;
    private final PostgresService postgresService;
    private final PowerStreamExecutor powerStreamExecutor;
    private final ExecutorService executorService;

    public TestService() throws SQLException {
        this.postgresService = new PostgresService();
        this.powerStreamExecutor = new PowerStreamExecutor();
        this.executorService = Executors.newFixedThreadPool(4);
    }

    public long executePowerTestAndReturnTime() throws SQLException {
        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        runPowerTest();
        stopwatch.stop();
        return stopwatch.getTime();
    }

    public long executeThroughputTestAndReturnTime() throws Exception {
        return runThroughputTest();
    }

    private void runPowerTest() throws SQLException {
        int refreshIterator = postgresService.getRefreshIterator();
        System.out.println("******************** " + refreshIterator);
        powerStreamExecutor.executePowerTest(refreshIterator);
    }

    private long runThroughputTest() throws Exception {
        int refreshIterator1 = postgresService.getRefreshIterator();
        System.out.println("******************** " + refreshIterator1);
        int refreshIterator2 = postgresService.getRefreshIterator();
        System.out.println("******************** " + refreshIterator2);

        QueryStreamCallable Q01 = new QueryStreamCallable("Q0");
        QueryStreamCallable Q02 = new QueryStreamCallable("Q1");
        RefreshStreamCallable R01 = new RefreshStreamCallable(refreshIterator1);
        RefreshStreamCallable R02 = new RefreshStreamCallable(refreshIterator2);

        List<Future<Long>> executionTimes = executorService.invokeAll(List.of(Q01, Q02, R01, R02));

        return executionTimes.stream().map(this::getTimeFromFuture).max(Long::compareTo).get();
    }

    private Long getTimeFromFuture(Future<Long> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}
