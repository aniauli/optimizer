package com.engineering.optimizer.testing;

import com.engineering.optimizer.database.PostgresService;

import java.sql.SQLException;

public class PowerStreamExecutor {

    public void executePowerTest(int refreshIterator) throws SQLException {
        PostgresService postgresService = new PostgresService();
        postgresService.executeFirstRefreshFunction(refreshIterator);
        postgresService.executeQueries();
        postgresService.executeSecondRefreshFunction(refreshIterator);
        postgresService.closeConnection();
    }
}
