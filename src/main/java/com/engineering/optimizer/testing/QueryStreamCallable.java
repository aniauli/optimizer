package com.engineering.optimizer.testing;

import com.engineering.optimizer.database.PostgresService;

import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.Callable;

public class QueryStreamCallable implements Callable<Long> {

    private final String streamName;

    QueryStreamCallable(String streamName)  {
        this.streamName = streamName;
        System.out.println("Creating thread with query stream name: " + streamName);
    }

    @Override
    public Long call() throws Exception {
        Instant startTime = Instant.now();
        try {
            PostgresService postgresService = new PostgresService();
            postgresService.executeQueries();
            System.out.println("Thread with query stream name: " + streamName + " is already finished");
            postgresService.closeConnection();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        Instant stopTime = Instant.now();
        return Duration.between(startTime, stopTime).toMillis();
    }
}
