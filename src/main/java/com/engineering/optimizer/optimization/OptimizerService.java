package com.engineering.optimizer.optimization;

import com.engineering.optimizer.testing.TestService;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.stereotype.Service;

import java.sql.SQLException;

import static com.engineering.optimizer.optimization.ConfigurationParameters.getUpdatedParameters;

@Service
public class OptimizerService {

    private final FilePostgresConfigurationService filePostgresConfigurationService;
    private final TestService testService;
    private final long optimalTime = 991401l;

    public OptimizerService() throws SQLException {
        this.testService = new TestService();
        this.filePostgresConfigurationService = new FilePostgresConfigurationService();
    }

    public String runTestsWithChangedParameters(ParametersToUpdate parametersToUpdate) throws Exception {
        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(getUpdatedParameters(parametersToUpdate));
        long powerTestsTime = testService.executePowerTestAndReturnTime();
        long throughputTestsTime = testService.executeThroughputTestAndReturnTime();
        return DurationFormatUtils.formatDurationHMS(powerTestsTime + throughputTestsTime);
    }

    public String getOptimalTime() {
        return DurationFormatUtils.formatDurationHMS(optimalTime);
    }
}
