package com.engineering.optimizer.optimization;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FilePostgresConfigurationService {

    public static final String POSTGRES_CONF_PATH = "C:\\Program Files\\PostgreSQL\\10\\data\\postgresql.conf";

    public void writeRowsToPostgresConfigurationFile(Map<String, String> parametersWithValues) throws IOException {
        writeRowsToFile(createRowsForPostgresConfigurationFile(parametersWithValues));
    }

    private void writeRowsToFile(List<String> rows) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FilePostgresConfigurationService.POSTGRES_CONF_PATH));
        for (String row : rows) {
            bufferedWriter.write(row);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

    private List<String> createRowsForPostgresConfigurationFile(Map<String, String> parametersWithValues){
        List<String> rows = new ArrayList<>();
        for (Map.Entry<String, String> parameter : parametersWithValues.entrySet()) {
            rows.add(parameter.getKey() + " = " + parameter.getValue());
        }
        return rows;
    }
}
