package com.engineering.optimizer.optimization;

public final class ParametersToUpdate {

    private final long sharedBuffersValueInKB;
    private final long workMemValueInKB;
    private final long maintenanceWorkMemValueInKB;
    private final long effectiveCacheSizeValueInKB;
    private final long effectiveIOConcurrencyValue;
    private final long maxParallelWorkersValue;

    public static final long SHARED_BUFFERS_MIN = 128;
    public static final long SHARED_BUFFERS_MAX = 4000000;
    public static final long SHARED_BUFFERS_DEFAULT = 128000;
    public static final long WORK_MEM_MIN = 64;
    public static final long WORK_MEM_MAX = 1000000;
    public static final long WORK_MEM_DEFAULT = 4000;
    public static final long MAINTENANCE_WORK_MEM_MIN = 1000;
    public static final long MAINTENANCE_WORK_MEM_MAX = 1000000;
    public static final long MAINTENANCE_WORK_MEM_DEFAULT = 64000;
    public static final long EFFECTIVE_CACHE_SIZE_MIN = 8;
    public static final long EFFECTIVE_CACHE_SIZE_MAX = 8000000;
    public static final long EFFECTIVE_CACHE_SIZE_DEFAULT = 4000000;
    public static final long EFFECTIVE_IO_CONCURRENCY_MIN = 1;
    public static final long EFFECTIVE_IO_CONCURRENCY_MAX = 1000;
    public static final long EFFECTIVE_IO_CONCURRENCY_DEFAULT = 1;
    public static final long MAX_PARALLEL_WORKERS_MIN = 0;
    public static final long MAX_PARALLEL_WORKERS_MAX = 1024;
    public static final long MAX_PARALLEL_WORKERS_DEFAULT = 8;

    public ParametersToUpdate(long sharedBuffersValueInKB, long workMemValueInKB,
                              long maintenanceWorkMemValueInKB, long effectiveCacheSizeValueInKB,
                              long effectiveIOConcurrencyValue, long maxParallelWorkersValue) {
        this.sharedBuffersValueInKB = sharedBuffersValueInKB;
        this.workMemValueInKB = workMemValueInKB;
        this.maintenanceWorkMemValueInKB = maintenanceWorkMemValueInKB;
        this.effectiveCacheSizeValueInKB = effectiveCacheSizeValueInKB;
        this.effectiveIOConcurrencyValue = effectiveIOConcurrencyValue;
        this.maxParallelWorkersValue = maxParallelWorkersValue;
    }

    public static ParametersToUpdate ofPercentage(long sharedBuffersPercentage, long workMemPercentage,
                                           long maintenanceWorkMemPercentage, long effectiveCacheSizePercentage,
                                           long effectiveIOConcurrencyPercentage, long maxParallelWorkersPercentage){
        return new ParametersToUpdate(
                transformFromPercentage(sharedBuffersPercentage, SHARED_BUFFERS_MIN, SHARED_BUFFERS_MAX),
                transformFromPercentage(workMemPercentage, WORK_MEM_MIN, WORK_MEM_MAX),
                transformFromPercentage(maintenanceWorkMemPercentage, MAINTENANCE_WORK_MEM_MIN, MAINTENANCE_WORK_MEM_MAX),
                transformFromPercentage(effectiveCacheSizePercentage, EFFECTIVE_CACHE_SIZE_MIN, EFFECTIVE_CACHE_SIZE_MAX),
                transformFromPercentage(effectiveIOConcurrencyPercentage, EFFECTIVE_IO_CONCURRENCY_MIN, EFFECTIVE_IO_CONCURRENCY_MAX),
                transformFromPercentage(maxParallelWorkersPercentage, MAX_PARALLEL_WORKERS_MIN, MAX_PARALLEL_WORKERS_MAX));
    }

    public long getSharedBuffersValueInKB() {
        return sharedBuffersValueInKB;
    }

    public long getWorkMemValueInKB() {
        return workMemValueInKB;
    }

    public long getMaintenanceWorkMemValueInKB() {
        return maintenanceWorkMemValueInKB;
    }

    public long getEffectiveCacheSizeValueInKB() {
        return effectiveCacheSizeValueInKB;
    }

    public long getEffectiveIOConcurrencyValue() {
        return effectiveIOConcurrencyValue;
    }

    public long getMaxParallelWorkersValue() {
        return maxParallelWorkersValue;
    }

    public static long transformFromPercentage(long parameterPercentage, long parameterMin, long parameterMax) {
        return (int) (parameterMin + (parameterMax - parameterMin)*(1.0*parameterPercentage/1000));
    }

    public static int transformToPercentage(long parameterDefault, long parameterMin, long parameterMax) {
        return (int) (1000*(parameterDefault - parameterMin)/(parameterMax - parameterMin));
    }
}
