package com.engineering.optimizer.optimization;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConfigurationParameters {

    public static final String SHARED_BUFFERS = "shared_buffers";
    public static final String WORK_MEM = "work_mem";
    public static final String MAINTENANCE_WORK_MEM = "maintenance_work_mem";
    public static final String EFFECTIVE_CACHE_SIZE = "effective_cache_size";
    public static final String EFFECTIVE_IO_CONCURRENCY = "effective_io_concurrency";
    public static final String MAX_PARALLEL_WORKERS = "max_parallel_workers";

    public static final Map<String, String> STATIC_DATA = Stream.of(new String[][] {
            {"listen_addresses", "'*'"},
            {"port", "5432"},
            {"max_connections", "100"},
            {"dynamic_shared_memory_type", "windows"},
            {"log_destination", "'stderr'"},
            {"logging_collector", "on"},
            {"log_timezone", "'Europe/Sarajevo'"},
            {"datestyle", "'iso, dmy'"},
            {"timezone", "'Europe/Sarajevo'"},
            {"lc_messages", "'Polish_Poland.1250'"},
            {"lc_monetary", "'Polish_Poland.1250'"},
            {"lc_numeric", "'Polish_Poland.1250'"},
            {"lc_time", "'Polish_Poland.1250'"},
            {"default_text_search_config", "'pg_catalog.simple'"}
    }).collect(Collectors.toMap(parameter -> parameter[0], parameter -> parameter[1]));

    public static Map<String, String> getUpdatedParameters(ParametersToUpdate toUpdate) {

        Map<String, String> updatedMap = new HashMap<>(STATIC_DATA);

        updatedMap.put(SHARED_BUFFERS, toUpdate.getSharedBuffersValueInKB() + "kB");
        updatedMap.put(WORK_MEM, toUpdate.getWorkMemValueInKB() + "kB");
        updatedMap.put(MAINTENANCE_WORK_MEM, toUpdate.getMaintenanceWorkMemValueInKB() + "kB");
        updatedMap.put(EFFECTIVE_CACHE_SIZE, toUpdate.getEffectiveCacheSizeValueInKB() + "kB");
        updatedMap.put(EFFECTIVE_IO_CONCURRENCY, String.valueOf(toUpdate.getEffectiveIOConcurrencyValue()));
        updatedMap.put(MAX_PARALLEL_WORKERS, String.valueOf(toUpdate.getMaxParallelWorkersValue()));

        return updatedMap;
    }

    public static Map<String, String> getDefaultParameters(){
        Map<String, String> updatedMap = new HashMap<>(STATIC_DATA);
        updatedMap.put(SHARED_BUFFERS, "128MB");
        return updatedMap;
    }
}
