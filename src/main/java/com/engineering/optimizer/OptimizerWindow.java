package com.engineering.optimizer;

import com.engineering.optimizer.optimization.OptimizerService;
import com.engineering.optimizer.optimization.ParametersToUpdate;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import static com.engineering.optimizer.optimization.ParametersToUpdate.*;
import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class OptimizerWindow {
    private JPanel mainPanel;
    private JLabel imageLabel;
    private JPanel imagePanel;
    private JPanel optimizerPanel;
    private JPanel sharedBuffersPanel;
    private JSlider sharedBuffersSlider;
    private JPanel workMemPanel;
    private JSlider workMemSlider;
    private JPanel maintenanceWorkMemPanel;
    private JSlider maintenanceWorkMemSlider;
    private JPanel effectiveCacheSizePanel;
    private JSlider effectiveCacheSizeSlider;
    private JPanel effectiveIoConcurrencyPanel;
    private JSlider effectiveIoConcurrencySlider;
    private JPanel maxParallelWorkersPanel;
    private JSlider maxParallelWorkersSlider;
    private JPanel submitPanel;
    private JButton submitButton;
    private JPanel testPanel;
    private JLabel testTimeLabel;
    private JPanel optimalTestPanel;
    private JLabel optimalTestTimeLabel;
    private JLabel sharedBuffersValueLabel;
    private JLabel workMemValueLabel;
    private JLabel maintenanceWorkMemValueLabel;
    private JLabel effectiveCacheSizeValueLabel;
    private JLabel effectiveIoConcurrencyValueLabel;
    private JLabel maxParallelWorkersValueLabel;
    private JButton defaultParametersButton;
    private JPanel defaultParametersPanel;
    private final JFrame frame;

    private final OptimizerService optimizerService;

    public OptimizerWindow() throws SQLException {
        optimizerService = new OptimizerService();
        frame = new JFrame("Postgres Configurator And Optimizer");
        mainPanel.setLayout(new GridLayout(1, 2));
        optimizerPanel.setLayout(new GridLayout(10, 1));
        imagePanel.setLayout(new GridLayout(1, 1));
        mainPanel.add(optimizerPanel);
        mainPanel.add(imagePanel);
        frame.add(mainPanel);
        frame.setMinimumSize(new Dimension(1000, 700));
        optimalTestTimeLabel.setText(optimizerService.getOptimalTime());

        setSlidersAndLabelsStartingValues();

        sharedBuffersSlider.addChangeListener(e -> sharedBuffersValueLabel
                .setText(format(transformFromPercentage(
                        sharedBuffersSlider.getValue(),
                        SHARED_BUFFERS_MIN,
                        SHARED_BUFFERS_MAX
                ))));

        workMemSlider.addChangeListener(e -> workMemValueLabel
                .setText(format(transformFromPercentage(
                        workMemSlider.getValue(),
                        WORK_MEM_MIN,
                        WORK_MEM_MAX
                ))));

        maintenanceWorkMemSlider.addChangeListener(e -> maintenanceWorkMemValueLabel
                .setText(format(transformFromPercentage(
                        maintenanceWorkMemSlider.getValue(),
                        MAINTENANCE_WORK_MEM_MIN,
                        MAINTENANCE_WORK_MEM_MAX
                ))));

        effectiveCacheSizeSlider.addChangeListener(e -> effectiveCacheSizeValueLabel
                .setText(format(transformFromPercentage(
                        effectiveCacheSizeSlider.getValue(),
                        EFFECTIVE_CACHE_SIZE_MIN,
                        EFFECTIVE_CACHE_SIZE_MAX
                ))));

        effectiveIoConcurrencySlider.addChangeListener(e -> effectiveIoConcurrencyValueLabel
                .setText(String.valueOf(transformFromPercentage(
                        effectiveIoConcurrencySlider.getValue(),
                        EFFECTIVE_IO_CONCURRENCY_MIN,
                        EFFECTIVE_IO_CONCURRENCY_MAX
                ))));

        maxParallelWorkersSlider.addChangeListener(e -> maxParallelWorkersValueLabel
                .setText(String.valueOf(transformFromPercentage(
                        maxParallelWorkersSlider.getValue(),
                        MAX_PARALLEL_WORKERS_MIN,
                        MAX_PARALLEL_WORKERS_MAX
                ))));

        submitButton.addActionListener(actionEvent -> {
            disableSliders();
            ParametersToUpdate parametersToUpdate = getValuesFromSliders();
            try {
                String time = optimizerService.runTestsWithChangedParameters(parametersToUpdate);
                testTimeLabel.setText(time);
            } catch (Exception e) {
                e.printStackTrace();
            }
            enableSliders();

        });

        defaultParametersButton.addActionListener(actionEvent -> {
            setSlidersAndLabelsStartingValues();
        });
    }

    public void start() {
        frame.setVisible(true);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private String format(long value) {
        if (0 < value / 1000000) {
            return String.format("%.2f", 1.0 * value / 1000000) + "GB";
        } else if (0 < value / 1000) {
            return String.format("%.2f", 1.0 * value / 1000) + "MB";
        } else {
            return value + "kB";
        }
    }

    private void setSlidersAndLabelsStartingValues() {
        sharedBuffersSlider.setValue(transformToPercentage(SHARED_BUFFERS_DEFAULT, SHARED_BUFFERS_MIN, SHARED_BUFFERS_MAX));
        sharedBuffersValueLabel.setText(format(SHARED_BUFFERS_DEFAULT));
        workMemSlider.setValue(transformToPercentage(WORK_MEM_DEFAULT, WORK_MEM_MIN, WORK_MEM_MAX));
        workMemValueLabel.setText(format(WORK_MEM_DEFAULT));
        maintenanceWorkMemSlider.setValue(transformToPercentage(MAINTENANCE_WORK_MEM_DEFAULT, MAINTENANCE_WORK_MEM_MIN, MAINTENANCE_WORK_MEM_MAX));
        maintenanceWorkMemValueLabel.setText(format(MAINTENANCE_WORK_MEM_DEFAULT));
        effectiveCacheSizeSlider.setValue(transformToPercentage(EFFECTIVE_CACHE_SIZE_DEFAULT, EFFECTIVE_CACHE_SIZE_MIN, EFFECTIVE_CACHE_SIZE_MAX));
        effectiveCacheSizeValueLabel.setText(format(EFFECTIVE_CACHE_SIZE_DEFAULT));
        effectiveIoConcurrencySlider.setValue(transformToPercentage(EFFECTIVE_IO_CONCURRENCY_DEFAULT, EFFECTIVE_IO_CONCURRENCY_MIN, EFFECTIVE_IO_CONCURRENCY_MAX));
        effectiveIoConcurrencyValueLabel.setText(String.valueOf(EFFECTIVE_IO_CONCURRENCY_DEFAULT));
        maxParallelWorkersSlider.setValue(transformToPercentage(MAX_PARALLEL_WORKERS_DEFAULT, MAX_PARALLEL_WORKERS_MIN, MAX_PARALLEL_WORKERS_MAX));
        maxParallelWorkersValueLabel.setText(String.valueOf(MAX_PARALLEL_WORKERS_DEFAULT));
    }

    private void disableSliders() {
        sharedBuffersSlider.setEnabled(false);
        workMemSlider.setEnabled(false);
        maintenanceWorkMemSlider.setEnabled(false);
        effectiveCacheSizeSlider.setEnabled(false);
        effectiveIoConcurrencySlider.setEnabled(false);
        maxParallelWorkersSlider.setEnabled(false);
    }

    private void enableSliders() {
        sharedBuffersSlider.setEnabled(true);
        workMemSlider.setEnabled(true);
        maintenanceWorkMemSlider.setEnabled(true);
        effectiveCacheSizeSlider.setEnabled(true);
        effectiveIoConcurrencySlider.setEnabled(true);
        maxParallelWorkersSlider.setEnabled(true);
    }

    private ParametersToUpdate getValuesFromSliders() {
        return ParametersToUpdate.ofPercentage(
                sharedBuffersSlider.getValue(),
                workMemSlider.getValue(),
                maintenanceWorkMemSlider.getValue(),
                effectiveCacheSizeSlider.getValue(),
                effectiveIoConcurrencySlider.getValue(),
                maxParallelWorkersSlider.getValue());
    }
}
