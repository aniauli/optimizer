package com.engineering.optimizer;

import com.engineering.optimizer.database.PostgresService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.SQLException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class PostgresServiceTest {

    private PostgresService postgresService;

    @Before
    public void init() throws SQLException {
        postgresService = new PostgresService();
    }

    @Test
    public void ordersFromFileAreSavedAndDeletedFromDatabase() throws SQLException {
        postgresService.addRowsToOrdersFromFile("test");
        assertTrue(postgresService.findOrdersById(9).next());
        assertTrue(postgresService.findOrdersById(10).next());
        postgresService.deleteRowsFromOrdersAndLineitemByIdsFromFile("test");
        assertFalse(postgresService.findOrdersById(9).next());
        assertFalse(postgresService.findOrdersById(10).next());
    }

    @Test
    public void lineitemsFromFileAreSavedAndDeletedFromDatabase() throws SQLException {
        postgresService.addRowsToLineitemFromFile("test");
        assertTrue(postgresService.findLineitemsById(9).next());
        assertTrue(postgresService.findLineitemsById(10).next());
        postgresService.deleteRowsFromOrdersAndLineitemByIdsFromFile("test");
        assertFalse(postgresService.findLineitemsById(9).next());
        assertFalse(postgresService.findLineitemsById(10).next());
    }

    @After
    public void teardown() {
        postgresService.closeConnection();
    }
}
