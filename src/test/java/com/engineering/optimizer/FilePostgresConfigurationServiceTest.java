package com.engineering.optimizer;

import com.engineering.optimizer.optimization.FilePostgresConfigurationService;
import com.engineering.optimizer.optimization.ParametersToUpdate;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.engineering.optimizer.optimization.ConfigurationParameters.getDefaultParameters;
import static com.engineering.optimizer.optimization.ConfigurationParameters.getUpdatedParameters;
import static org.junit.jupiter.api.Assertions.*;

public class FilePostgresConfigurationServiceTest {

    private static final String POSTGRES_CONF_PATH = "C:\\Program Files\\PostgreSQL\\10\\data\\postgresql.conf";
    private final FilePostgresConfigurationService filePostgresConfigurationService = new FilePostgresConfigurationService();

    @Test
    public void postgresConfigurationFileIsOverwritten() throws IOException {
        Map<String, String> testParametersWithValues = Stream.of(new String[][] {
                {"testParameter1", "testValue1"},
                {"testParameter2", "testValue2"},
                {"testParameter3", "testValue3"}
        }).collect(Collectors.toMap(parameter -> parameter[0], parameter -> parameter[1]));

        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(testParametersWithValues);
        List<String> newRows = FileUtils.getRowsFromFile(POSTGRES_CONF_PATH);
        assertEquals(3, newRows.size());
        assertEquals("testParameter1 = testValue1", newRows.get(0));
        assertEquals("testParameter2 = testValue2", newRows.get(1));
        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(getDefaultParameters());
    }

    @Test
    public void postgresConfigurationFileIsOverwrittenWithRealStaticData() throws IOException {
        ParametersToUpdate parametersToUpdate = new ParametersToUpdate(
                150,
                70,
                1,
                5,
                5,
                3);

        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(getUpdatedParameters(parametersToUpdate));
        List<String> newRows = FileUtils.getRowsFromFile(POSTGRES_CONF_PATH);
        assertEquals(20, newRows.size());
        assertTrue(newRows.contains("listen_addresses = '*'"));
        assertTrue(newRows.contains("lc_messages = 'Polish_Poland.1250'"));
        assertTrue(newRows.contains("shared_buffers = 150kB"));
        assertTrue(newRows.contains("effective_io_concurrency = 5"));
        assertTrue(newRows.contains("maintenance_work_mem = 1MB"));
        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(getDefaultParameters());
    }

    @Test
    public void postgresConfigurationFileIsOverwrittenWithDefaultData() throws IOException {
        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(getDefaultParameters());
        List<String> newRows = FileUtils.getRowsFromFile(POSTGRES_CONF_PATH);
        assertEquals(15, newRows.size());
        assertTrue(newRows.contains("listen_addresses = '*'"));
        assertTrue(newRows.contains("lc_messages = 'Polish_Poland.1250'"));
        assertTrue(newRows.contains("shared_buffers = 128MB"));
        assertFalse(newRows.contains("effective_io_concurrency"));
        assertFalse(newRows.contains("maintenance_work_mem"));
        filePostgresConfigurationService.writeRowsToPostgresConfigurationFile(getDefaultParameters());
    }

}
