package com.engineering.optimizer;

import org.junit.Test;

import static com.engineering.optimizer.optimization.ParametersToUpdate.*;
import static java.lang.Math.abs;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigurationParametersTest {

    @Test
    public void transformToPercentageWorks(){
        long sharedBuffers = 128000;
        long workMem = 4000;
        long maintenanceWorkMem = 64000;
        long effectiveCacheSize = 4000000;
        long effectiveIoConcurrency = 1;
        long maxParallelWorkers = 8;

        assertTrue(abs(32 - transformToPercentage(sharedBuffers, SHARED_BUFFERS_MIN, SHARED_BUFFERS_MAX)) < 2);
        assertTrue(abs(4 - transformToPercentage(workMem, WORK_MEM_MIN, WORK_MEM_MAX)) < 2);
        assertTrue(abs(64.1 - transformToPercentage(maintenanceWorkMem, MAINTENANCE_WORK_MEM_MIN, MAINTENANCE_WORK_MEM_MAX)) < 2);
        assertTrue(abs(500.1 - transformToPercentage(effectiveCacheSize, EFFECTIVE_CACHE_SIZE_MIN, EFFECTIVE_CACHE_SIZE_MAX)) < 2);
        assertTrue(abs(1 - transformToPercentage(effectiveIoConcurrency, EFFECTIVE_IO_CONCURRENCY_MIN, EFFECTIVE_IO_CONCURRENCY_MAX)) < 2);
        assertTrue(abs(7.8 - transformToPercentage(maxParallelWorkers, MAX_PARALLEL_WORKERS_MIN, MAX_PARALLEL_WORKERS_MAX)) < 2);
    }

    @Test
    public void transformFromPercentageWorks(){
        int sharedBuffers = 32;
        int workMem = 4;
        int maintenanceWorkMem = 64;
        int effectiveCacheSize = 500;
        int effectiveIoConcurrency = 1;
        int maxParallelWorkers = 8;

        assertTrue(abs(128000 - transformFromPercentage(sharedBuffers, SHARED_BUFFERS_MIN, SHARED_BUFFERS_MAX)) < 200);
        assertTrue(abs(4000 - transformFromPercentage(workMem, WORK_MEM_MIN, WORK_MEM_MAX)) < 200);
        assertTrue(abs(64000 - transformFromPercentage(maintenanceWorkMem, MAINTENANCE_WORK_MEM_MIN, MAINTENANCE_WORK_MEM_MAX)) < 1000);
        assertTrue(abs(4000000 - transformFromPercentage(effectiveCacheSize, EFFECTIVE_CACHE_SIZE_MIN, EFFECTIVE_CACHE_SIZE_MAX)) <200);
        assertTrue(abs(1 - transformFromPercentage(effectiveIoConcurrency, EFFECTIVE_IO_CONCURRENCY_MIN, EFFECTIVE_IO_CONCURRENCY_MAX)) < 200);
        assertTrue(abs(8 - transformFromPercentage(maxParallelWorkers, MAX_PARALLEL_WORKERS_MIN, MAX_PARALLEL_WORKERS_MAX)) < 200);
    }

}
